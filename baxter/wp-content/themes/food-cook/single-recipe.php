<?php
// File Security Check
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) :
	die ( 'You do not have sufficient permissions to access this page!' );
endif;

/**
* Single Custom Post Template
*
* This template is the default page template. It is used to display content when someone is viewing a
* singular view of a post ('post' post_type).
* @link http://codex.wordpress.org/Post_Types#Post
*
* @package WooFramework
* @subpackage Template
*/
global $woo_options; ?>

<?php get_header(); ?>

	<!-- #content Starts -->
	<?php woo_content_before(); ?>

	<div id="content" class="col-full">

		<div id="main-sidebar-container">    

			<!-- #main Starts -->
			<?php woo_main_before(); ?>

			<div id="main" class="col-left post single-recipe"> 
			
			<?php
				$recipe_images 	= get_post_meta( get_the_id(), 'RECIPE_META_more_images_recipe' );
				$embed_code_top = get_post_meta( get_the_id(), 'RECIPE_META_video_embed_top', true );
				$images_count 	= count( $recipe_images );

				if( $images_count > 0 ) {

					echo "<section class='slider'><div id='slider' class='featured-slider owl-carousel'>";

					foreach($recipe_images as $image) {
						echo wp_get_attachment_image($image, 'full', false, array( 'class' => 'photo' ));
					} 

					echo "</div></section>";
					df_meta_image();

				} else if(!empty($embed_code_top)) {

					echo "<div class='single-img-box'>";
					echo $embed_code_top;
					echo "</div>";
					df_meta_image();

			 	} else {

					get_the_image( array(
						'post_id' 		=> get_the_ID(),
						'order'   		=> array( 'featured', 'default' ),
						'featured'  	=> true,
						'default' 		=> esc_url( get_template_directory_uri() . '/includes/assets/images/image.jpg' ),
						'size'			=> 'full',
						'link_to_post'  => false,
						'before'        => '<div class="single-img-box photo">',
						'after'         => '</div>'
					) );

				} 
			?>
				
				<?php while (have_posts()) : the_post(); ?>
	
				<h1 class="title fn" itemprop="name" ><?php the_title(); ?></h1>
				
				<?php df_postmeta_recipe(); ?>

				<?php df_taxonomies_info(); ?>

				<div class="col-12 col-md-6 col-lg-12">
                      <div class="mt-5 mt-md-0 mt-lg-5">
              <?php echo get_post_meta($id, "boxter_heading", true); ?>

           <?php 

            $image_id = get_post_meta($id,'image');
echo get_the_post_thumbnail($image_id);

?>

 <?php

      $attachment_ids= get_post_meta( $id, 'image', true );

  ?>
                                                <div class="mt-2 recipe-product-thumb">
<?php       foreach((array)$attachment_ids AS $attachment_id){

?>
    <?php echo wp_get_attachment_image($attachment_ids, 'medium');?>
</div><?php } ?>

<?php $mylink = get_post_meta($id, 'url', true);

?>
                </div>
                                                <a href="<?php echo $mylink ?>" class="my-3 btn btn-outline-secondary btn-skinny btn--icon-right">
                  View product
                  <svg class="arrow icon" xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48">
  <path fill="currentColor" d="M33.7234347,22.5622087 C30.0464399,18.2976005 28.5,15.531491 28.5,14.7696203 C28.5,14.3848101 28.8851852,14 29.3185185,14 C29.8962963,14 30.4740741,14.6734177 31.6777778,15.635443 L41.5,23.5240506 L31.6777778,31.364557 C30.4740741,32.3265823 29.8962963,33 29.3185185,33 C28.8851852,33 28.5,32.6632911 28.5,32.278481 C28.5,31.5121359 30.1069455,28.7179414 33.79641,24.4106006 C32.1258239,24.519872 29.5218831,25.1189872 26.8037225,26.5 C20.9763113,25.2432432 14.3908629,24.5540541 7,24.3918919 L7,22.6081081 L8.27918782,22.6081081 C14.0592217,22.6081081 20.2182741,21.8783784 26.8037225,20.5 C29.1831091,21.7078139 31.5968241,22.386882 33.7234347,22.5622087 Z"></path>
</svg>
                </a>
                                          </div>
                  </div>

				<div class="content-left-first boxinc entry-content" itemprop="description">	


						<?php the_content(); ?>
					

				</div>

				<?php endwhile; ?>
				
				<div class="content-right-first info-right ">	
					
					<?php df_rating_info(); ?>
				
				</div><!-- end of info-right div -->

				<?php df_extra_share_recipe(); ?>

				<?php df_recipe_tabs(); ?>

				<?php df_below_content_social_recipe(); ?>
 
				<?php df_cook_info_recipe(); ?>

				<?php df_related_recipe(); ?>
			</div><!-- /#main -->

			<?php woo_main_after(); ?>

			<?php get_sidebar(); ?>

		</div><!-- /#main-sidebar-container -->         

        <?php dahz_get_sidebar( 'secondary' ); ?>

	</div><!-- /#content -->

	<?php woo_content_after(); ?>

<?php get_footer(); ?>