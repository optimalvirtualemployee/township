<?php
/**
 * Single Product Thumbnails
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-thumbnails.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     3.5.1
 */


if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $product;

$attachment_ids = $product->get_gallery_image_ids();

if ( $attachment_ids ) {
	?>
	<div class="product_thumbnails">

		<div class="product_thumbnails_swiper_container">
    		
        	<div class="swiper-wrapper">

     <?php
     if ( has_post_thumbnail() ) {
     $image_title 		= esc_attr( get_the_title( get_post_thumbnail_id() ) );
     $image       = get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_small_thumbnail_size', 'shop_thumbnail' ), 0, $attr = array(
				'title'	=> $image_title,
				'alt'	=> $image_title
				) );


      echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<div class="swiper-slide">%s</div>', $image ), $post->ID );     
	

	 foreach ( $attachment_ids as $attachment_id ) {

			$image_link = wp_get_attachment_url( $attachment_id );

			if ( ! $image_link )
				continue;

			$image_title 	= get_post_field( 'post_title', $attachment_id );
			$image_caption 	= get_post_field( 'post_excerpt', $attachment_id );

			$image       = wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_thumbnail' ), 0, $attr = array(
				'title'			=> $image_title,
				'alt'			=> $image_title,
				'data-caption'	=> get_post_field( 'post_excerpt', $attachment_id ),
				) );

			//echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<a href="%s" class="%s" title="%s" data-rel="prettyPhoto[product-gallery]">%s</a>', $image_link, $image_class, $image_caption, $image ), $attachment_id, $post->ID, $image_class );
            echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<div class="swiper-slide">%s</div>', $image ), $attachment_id, $attachment_id );

		}	

		} ?>

			</div> <!-- .swiper-wrapper -->
					<div class="swiper-scrollbar"></div>
		</div> <!-- .product_thumbnails_swiper_container -->

	</div> <!-- .product_thumbnails -->
	<?php
}
