<?php

// JSON-LD for Wordpress Home Articles and Author Pages written by Pete Wailes and Richard Baxter
function get_post_data() { global $post; return $post; } 

$ldsingleRecipe["@context"] = "http://schema.org/";

// this has all the data of the post/page etc 
$post_data = get_post_data(); // stuff for any page, if it exists 
$category = get_the_category(); // stuff for specific pages 
$blog_title = get_bloginfo( 'name' );
// var_dump($post_data);
if ( is_singular( 'recipe' ) ) {
	// this gets the data for the user who wrote that particular item 
	$author_data = get_userdata($post_data->post_author); 
	$post_url = get_permalink(); 
	$post_thumb = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())); 
	$postThumbmetasize = wp_get_attachment_metadata(get_post_thumbnail_id(get_the_ID()), 'true');
	$prep_time =  woo_fnc_convert_to_hours(get_post_meta(get_the_id(), 'RECIPE_META_prep_time', true));
	$pt = 'PT'.$prep_time.'M';
	$cook_time =  woo_fnc_convert_to_hours(get_post_meta(get_the_id(), 'RECIPE_META_cook_time', true));
	$ct = 'PT'.$cook_time.'M';
	$yield = get_post_meta(get_the_id(), 'RECIPE_META_yield', true);

    $ingredients = get_post_meta($post_data->ID, 'RECIPE_META_ingredients');
    $ingredients_count = count($ingredients[0]);

    $vote_count = woo_fnc_get_cont_rating(get_the_id());
	$ingredients_html = '';
    if ($ingredients_count >= 1) {

        foreach ($ingredients as $key) {
            $ingredients_html .= implode( ", ", $key);
        }

    } else {
        $ingredients_html .= '';
    }

    $servings = get_post_meta(get_the_id(), 'RECIPE_META_servings', true);

	$nut_names = get_post_meta(get_the_id(), 'RECIPE_META_nut_name');
	$nut_number = 0;

		if(is_array($nut_names)){
			$nut_number = count($nut_names[0]);
		}

		if($nut_number >= 1){
			$nut_vals = get_post_meta(get_the_id(), 'RECIPE_META_nut_mass');
			$valuesOfNutrions = '';
			for ($i=0; $i < $nut_number; $i++) { 
				$valuesOfNutrions .= $nut_vals[0][$i] . ' ' . $nut_names[0][$i] . ',' ;
			}
		}

	$ldsingleRecipe["@type"] = "Recipe";
	$ldsingleRecipe["name"] = $post_data->post_title;
	$ldsingleRecipe["image"] = $post_thumb;
	$ldsingleRecipe["datePublished"] = $post_data->post_date;
	$ldsingleRecipe["description"] = $post_data->post_content;
	$ldsingleRecipe["prepTime"] = $pt;
	$ldsingleRecipe["totalTime"] = $ct;
	$ldsingleRecipe["recipeYield"] = $yield;
	$ldsingleRecipe["recipeIngredient"] = explode(", ", $ingredients_html);
	$ldsingleRecipe["author"] = array( "@type" => "Person", "name" => $author_data->display_name, );
	$ldsingleRecipe["aggregateRating"] = array( "@type" => "AggregateRating", "ratingValue" => woo_fnc_get_avg_rating(get_the_id()), "reviewCount" => $vote_count );
	$ldsingleRecipe["nutrition"] = array( "@type" => "NutritionInformation", "servingSize" => $servings, "calories" => array_filter(explode(',', !empty( $valuesOfNutrions ) ? $valuesOfNutrions : '' ) ) );

}