<?php 
/*
* Template Name: Recipes Grid Template 
*/ 
get_header();
global $woo_options;

?>
       
    <!-- #content Starts -->
	<?php woo_content_before(); ?>
    <div id="content" class="col-full">

<style type="text/css">
	/* Search & Filter Styles */
body
{
	display:block;
}
.searchandfilter p
{
	margin-top: 1em;
	display:inline-block;
}
.searchandfilter ul
{
	display:inline-block;
}
.searchandfilter li
{
	list-style: none;
	display:inline-block;
	padding-right:10px;
}

/* If in a widget area make a single column by adding display block  */
.widget-area .searchandfilter li, .widget-area .searchandfilter p
{
	display:block;
}
.searchandfilter label
{
	display:block;
}
.searchandfilter h4
{
	margin:15px 0;
	font-size:16px;
}

.searchandfilter ul > li > ul:not(.children)
{
	margin-left:0;
}
</style>
    	<?php echo do_shortcode( '[searchandfilter fields="search"]' ); ?>
    	<?php echo do_shortcode( '[searchandfilter fields="recipe_type,course" types="radio,radio" hierarchical=",1" submit_label="Filter"]' ); ?>									



<!-- [searchandfilter fields="search,category,post_tag,post_format,recipe-category,recipe-cuisine,recipe-cooking-method,recipe-tag,recipe_type,cuisine,course,ingredient,skill_level,calories,product_cat,product_tag,product_shipping_class,slide-page"]
 -->


    	<div id="main-sidebar-container">    
            <!-- #main Starts -->
            <?php woo_main_before(); ?>
            <div id="main" class="col-left">                     
		 
			<?php woo_loop_before();?>
             	<?php 
 			 
             	if ( $woo_options['woo_slider_recipe'] == 'true') { 
             		 dahz_get_template( 'sliders', 'slider' );  
				} 
             	?>
    		<?php wp_reset_query(); ?>

			    <div class="orderby">
	            	<form class="recipe-ordering"  method="post" id="order">
					    <button type="submit" name="asc"  title="Ascending"><i class="fa fa-angle-double-up"></i></button>
					  <button type="submit" name="desc" value="desc" title="Descending"><i class="fa fa-angle-double-down"></i></button>
					 
					  <select name="select">
					    <option value="Select Option"><?php _e( 'Sort by', 'woothemes' );  ?></option>
					 
					    <option value="date"><?php _e( 'Date', 'woothemes' );  ?></option>
					    <option value="name"><?php _e( 'Name', 'woothemes' );  ?></option>
					    <option value="comment_count"><?php _e( 'Popular', 'woothemes' );  ?></option>

					  </select>
					
					</form>
				</div>

				<div class="recipe-title">
					<h1 class="title"><?php the_title(); ?> </h1> 
            	</div>
				
				<div class="fix"></div>

					<?php dahz_get_template('loop', 'loop-recipe'); ?>

				<div class="fix"></div>
	   
            </div><!-- /#main -->
            <?php woo_main_after(); ?>
    
            <?php get_sidebar(); ?>

		</div><!-- /#main-sidebar-container -->         

		<?php dahz_get_sidebar( 'secondary' ); ?>

    </div><!-- /#content -->
	<?php woo_content_after(); ?>

<?php get_footer(); ?>